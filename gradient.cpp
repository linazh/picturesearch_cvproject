#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <cmath>
#include <string>

using namespace cv;

int main() {

	Mat imgOrig = imread("img0.jpg", 0), src;
	imgOrig.copyTo(src);
	Mat dstX, dstY, dstAbsX, dstAbsY;
	Mat dstGrad(src.size(), src.type()), dstAngle(src.size(), src.type());

	string imName = "Image";
	string GradXname = "SobelX", GradYname = "SobelY", GradName = "Gradient", addname;

	int ordDer = 1, ord0 = 0; //order of derivate from 0 to 2; only one(!) dimension can equal 0;
	char der = '0' + ordDer;

	// *** Gradient with Sobel Operator ***

	for (int k = 0; k < 2; ++k){

		if (k == 1){
			// make blured image
			addname = "_blurred";
			medianBlur(imgOrig, src, 5);
		}

		// show image

		namedWindow(imName + addname);
		imshow(imName + addname, src);
		waitKey(0);

		// X derivate
		Sobel(src, dstX, src.depth(), ordDer, ord0);
		namedWindow(GradXname + addname);
		imshow(GradXname + addname, dstX);

		// Y derivate
		Sobel(src, dstY, src.depth(), ord0, ordDer);
		namedWindow(GradYname + addname);
		imshow(GradYname + addname, dstY);

		waitKey(0);

		// work out Gradient
		for (int i = 0; i < src.rows; ++i){
			for (int j = 0; j < src.cols; ++j){
				int x = (dstX.at<uchar>(i, j));
				int y = (dstY.at<uchar>(i, j));
				dstGrad.at<uchar>(i, j) = sqrt(x * x + y * y);
			}
		}

		namedWindow(GradName + addname);
		imshow(GradName + addname, dstGrad);

		waitKey(0);

		// save images
		imwrite(imName + addname + ".jpg", src);
		imwrite(GradXname + "_der" + der + addname + ".jpg", dstX);
		imwrite(GradYname + "_der" + der + addname + ".jpg", dstY);
		imwrite(GradName + "_der" + der + addname + ".jpg", dstGrad);
	}

	return 0;
}