#include <opencv2\highgui\highgui.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\nonfree\nonfree.hpp>
#include <iostream>
#include <fstream>
#include <string>

using namespace cv;
using namespace std;

int main() {
	string dir_name = "C:\\Users\\alina_000\\Documents\\Visual Studio 2013\\Projects\\myCVproject\\__data\\";
	string dir_descriptors = "descriptors\\";
	string dir_clustering = "clustering\\";
	string dir_TFIDF = "tf-idf\\";

	ifstream finDataSize(dir_name + "_dataSize.txt");
	ifstream finMatrixSizes(dir_name + dir_descriptors + "_AllMatrixSizes.txt");
	ifstream finClusterNums(dir_name + dir_clustering + "All_clusters.txt");
	ifstream finClusterCenters(dir_name + dir_clustering + "centers_of_clusters.txt");

	int dataSize, numLines, numClusters; // number of clusters == number of different words
	finDataSize >> dataSize;
	finClusterNums >> numLines;
	finClusterCenters >> numClusters;

	vector <double> templ(numClusters + 1, 0); // "template" vector
	vector <vector <double> > TF(dataSize, templ); // for each doc, for each word -- |word i| / |num of words in doc|
	vector <double> IDF = templ; // for each word -- log ( |dataSize| / |frequency of word i| ) 
	vector <vector <int> > Words_Frequency(numClusters + 1); // for each word -- numbers of documents where the word is in
	vector <vector <double> > TF_IDF(dataSize, templ);


	int idocSize, word;
	vector <double> idoc_WordsNum; // for each word in doc i -- number of the word in the doc

	// writing in files worked out values 

	ofstream foutTF(dir_name + dir_TFIDF + "TF.txt");
	ofstream foutIDF(dir_name + dir_TFIDF + "IDF.txt");
	ofstream foutWordsF(dir_name + dir_TFIDF + "Words_Frequency.txt");
	ofstream foutTFIDF(dir_name + dir_TFIDF + "TF-IDF.txt");

	foutTF << TF.size() << " " << TF[0].size() << endl;
	foutIDF << IDF.size() << endl;
	foutWordsF << Words_Frequency.size() << endl;
	foutTFIDF << TF_IDF.size() << " " << TF_IDF[0].size() << endl;

	// work out TF for every document and every word

	for (int idoc = 0; idoc < dataSize; ++idoc) {
		// document with number i

		cout << idoc << " TF \n";

		finMatrixSizes >> idocSize;

		idoc_WordsNum = templ;
		vector <bool> counted(numClusters + 1, false);

		for (int j = 0; j < idocSize; ++j) {
			finClusterNums >> word;
			idoc_WordsNum[word]++;
			if (!counted[word]) {
				counted[word] = true;
				Words_Frequency[word].push_back(idoc);
			}
		}

		for (int word = 0; word <= numClusters; ++word) {
			TF[idoc][word] = idoc_WordsNum[word] / idocSize;
		
			foutTF << TF[idoc][word] << ' ';
		}

		foutTF << endl;
	}

	// work out IDF for every word 

	for (word = 0; word <= numClusters; ++word) {
		
		cout << word << " IDF \n";

		if (Words_Frequency[word].size() > 0) {
			IDF[word] = log(dataSize / Words_Frequency[word].size());
			
			foutIDF << IDF[word] << " ";
		}

		foutIDF << endl;
	}

	// work out TF-IDF for every pair (doc i, word j)

	for (int idoc = 0; idoc < dataSize; ++idoc) {

		cout << idoc << " TF-IDF \n";

		for (word = 0; word <= numClusters; ++word) {
			TF_IDF[idoc][word] = TF[idoc][word] * IDF[word];
		
			foutTFIDF << TF_IDF[idoc][word] << " ";
		}

		foutTFIDF << endl;
	}

	// write frequency of words

	for (int i = 0; i <= numClusters; ++i) {
		
		cout << i << " ith wordF \n";
		
		foutWordsF << Words_Frequency[i].size() << endl;

		for (int j = 0; j < Words_Frequency[i].size(); ++j) {
			foutWordsF << Words_Frequency[i][j] << " ";
		}

		foutWordsF << endl;
	}

	return 0;
}