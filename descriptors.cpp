#include <opencv2\core\core.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\nonfree\features2d.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\nonfree\nonfree.hpp>
#include <iostream>
#include <fstream>
#include <string>

using namespace cv;
using namespace std;


int main() {
	
	string dir_name = "C:\\Users\\alina_000\\Documents\\Visual Studio 2013\\Projects\\myCVproject\\__data\\";
	string dir_keyPt = "key points\\";
	string dir_descriptors = "descriptors\\";
	
	ifstream fin("img_names.txt");
	ofstream foutAll(dir_name + dir_descriptors + "All_descriptors.txt");
	ofstream foutEverySize(dir_name + dir_descriptors + "_AllMatrixSizes.txt"); // file with sizes of every descriptor matrix

	string img_name;
	int num = 0;
	int numberOfLines = 0;

	while (fin >> img_name) {
		num++;
		
		Mat img = imread(dir_name + img_name);
		vector <KeyPoint> keypoints;
		Mat descriptors;

		SiftFeatureDetector detector(400);
		detector.detect(img, keypoints);

		SiftDescriptorExtractor extractor;
		extractor.compute(img, keypoints, descriptors);

		ofstream fout(dir_name + dir_keyPt + "keypoints_" + to_string(num) + ".txt");
		ofstream fout2(dir_name + dir_descriptors + "descriptors_" + to_string(num) + ".txt");

		cout << num << ") " << keypoints.size() << " " << descriptors.size() << " " << img.size() << endl;
		
		fout << keypoints.size() << endl;
		fout2 << descriptors.rows << " " << descriptors.cols << endl;
		foutEverySize << descriptors.rows << endl;

		numberOfLines += descriptors.rows;

		for (int i = 0; i < keypoints.size(); ++i){
			KeyPoint kpt = keypoints[i];
			// 7 characteristics of a KeyPoint:
			// x, y, size, angle, response, octave, class_id
			fout << kpt.pt.x << " " << kpt.pt.y << " " << kpt.size << " ";
			fout << kpt.angle << " " << kpt.response << " " << kpt.octave << " " << kpt.class_id << endl;
			
			// descriptor for i-th kpt is 128 float number line in descriptors matrix
			
			for (int j = 0; j < descriptors.cols; ++j){
				fout2 << descriptors.at<float>(i, j) << " ";
				foutAll << descriptors.at<float>(i, j) << " ";
			}
			fout2 << endl;
			foutAll << endl;
		}
		foutAll << endl;
	}

	// write the result size of matrix with all descriptors
	ofstream foutResSize(dir_name + dir_descriptors + "_BigMatrixSize.txt");
	foutResSize << numberOfLines << " " << 128 << endl;

	ofstream foutDataSize(dir_name + "_dataSize.txt");
	foutDataSize << num << endl;
	
	return 0;
}
