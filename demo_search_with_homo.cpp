#include <opencv2\opencv.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\nonfree\features2d.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\nonfree\nonfree.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>
#include <set>


using namespace cv;
using namespace std;


void calculation_closest_clusters(Mat &centers, Mat &data, vector <int> &labels) {

	const int begin = 0;
	const int end = data.rows;
	const int K = centers.rows;
	const int dims = centers.cols;

	for (int i = begin; i<end; ++i)
	{
		const float *sample = data.ptr<float>(i);
		int k_best = 0;
		double min_dist = DBL_MAX;

		for (int k = 0; k < K; k++)
		{
			const float* center = centers.ptr<float>(k);
			const double dist = normL2Sqr_(sample, center, dims);

			if (min_dist > dist)
			{
				min_dist = dist;
				k_best = k;
			}
		}

		labels[i] = k_best;
	}
}

long double calculation_homography_distance(Mat const &request, vector <KeyPoint> &keypoints_request, Mat &descriptors_request, Mat const &candidate) {
	/*
	string dir_name = "C:\\Users\\alina_000\\Documents\\Visual Studio 2013\\Projects\\myCVproject\\__trySIFTdata\\";
	string dir_descriptors = "descriptors400\\";
	string dir_keypoints = "key points400\\";
	*/

	SiftFeatureDetector detector(400);
	vector<KeyPoint> keypoints_candidate;
	detector.detect(candidate, keypoints_candidate);

	SiftDescriptorExtractor extractor;
	Mat descriptors_candidate;
	extractor.compute(candidate, keypoints_candidate, descriptors_candidate);

	FlannBasedMatcher matcher;
	vector<DMatch> matches_vector;
	matcher.match(descriptors_candidate, descriptors_request, matches_vector);

	double max_dist = 0; double min_dist = 100;

	//-- Quick calculation of max and min distances between keypoints
	for (int i = 0; i < descriptors_candidate.rows; i++) {
		double dist = matches_vector[i].distance;
		if (dist < min_dist) min_dist = dist;
		if (dist > max_dist) max_dist = dist;
	}

	printf("-- Max dist : %f \n", max_dist);
	printf("-- Min dist : %f \n", min_dist);

	if (max_dist == 0) { return 0; } // perfect match!!!

	//-- Draw only "good" matches (i.e. whose distance is less than 3*min_dist )
	vector <DMatch> good_matches;

	for (int i = 0; i < matches_vector.size(); i++) {
		if (matches_vector[i].distance < 3 * min_dist) {
			good_matches.push_back(matches_vector[i]);
		}
	}

	cout << "*  |\n";

	Mat img_matches;
	drawMatches(candidate, keypoints_candidate, request, keypoints_request,
		good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

	//-- Localize the object
	vector<Point2f> goodpt_request;
	vector<Point2f> goodpt_candidate;

	for (int i = 0; i < (int)good_matches.size(); i++)
	{
		//-- Get the keypoints from the good matches

		goodpt_candidate.push_back(keypoints_candidate[good_matches[i].queryIdx].pt);
		goodpt_request.push_back(keypoints_request[good_matches[i].trainIdx].pt);
	}

	cout << "HOMO\n";

	//// homography from potential result (aka candidate) to request
	
	Mat H = findHomography(goodpt_candidate, goodpt_request, CV_RANSAC);

/*	//-- Get the corners from the image_1 ( the object to be "detected" )
	std::vector<Point2f> obj_corners(4);
	obj_corners[0] = cvPoint(0, 0); obj_corners[1] = cvPoint(candidate.cols, 0);
	obj_corners[2] = cvPoint(candidate.cols, candidate.rows); obj_corners[3] = cvPoint(0, candidate.rows);
	std::vector<Point2f> scene_corners(4);

	perspectiveTransform(obj_corners, scene_corners, H);

	//-- Draw lines between the corners (the mapped object in the scene - image_2 )
	line(img_matches, scene_corners[0] + Point2f(request.cols, 0), scene_corners[1] + Point2f(request.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[1] + Point2f(request.cols, 0), scene_corners[2] + Point2f(request.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[2] + Point2f(request.cols, 0), scene_corners[3] + Point2f(request.cols, 0), Scalar(0, 255, 0), 4);
	line(img_matches, scene_corners[3] + Point2f(request.cols, 0), scene_corners[0] + Point2f(request.cols, 0), Scalar(0, 255, 0), 4);

	//-- Show detected matches
	//namedWindow("Good");
	//imshow("Good", img_matches);
	//waitKey(0);
	*/

	vector <Point2f> transform_candidate(goodpt_candidate.size());
	perspectiveTransform(goodpt_candidate, transform_candidate, H);

	long double deviation = 0, distX, distY;
	Point2f ptGood, ptTransform;

	for (int i = 0; i < goodpt_request.size(); ++i) {
		ptGood = goodpt_request[i];
		ptTransform = transform_candidate[i];

		distX = ptGood.x - ptTransform.x;
		distY = ptGood.y - ptTransform.y;
		deviation += (distX * distX + distY * distY);
	}

	return deviation;
}

int main() {
	string dir_name = "C:\\Users\\alina_000\\Documents\\Visual Studio 2013\\Projects\\myCVproject\\__trySIFTdata\\";
	string dir_descriptors = "descriptors400\\";
	string dir_clustering = "clustering400\\";
	string dir_TFIDF = "tf-idf400\\";
	string dir_TFIDF_bin = "tf-idf_bin\\";

	ifstream finDataSize(dir_name + "_dataSize.txt");
	ifstream finClusterCenters(dir_name + dir_clustering + "centers_of_clusters.txt");

	int dataSize, numLines, numClusters; // number of clusters == number of different words
	const int dataEnd = 6131;
	finDataSize >> dataSize;
	//dataSize = 5575;
	finClusterCenters >> numClusters;

	int descriptPar = 128; // characteristic of descriptors (for SIFT 128 float numbers)
	Mat centers_of_clusters(numClusters, descriptPar, CV_32F);

	for (int i = 0; i < numClusters; ++i) {
		for (int j = 0; j < descriptPar; ++j) {
			finClusterCenters >> centers_of_clusters.at<float>(i, j);
		}
	}

	vector <double> templ(numClusters, 0); // "template" vector
	//vector <vector <double> > TF(dataSize, templ); // for each doc, for each word -- |word i| / |num of words in doc|
	vector <double> IDF = templ; // for each word -- log ( |dataSize| / |frequency of word i| ) 
	vector <vector <int> > Words_Frequency(numClusters); // for each word -- numbers of documents where the word is in
	vector <vector <double> > TF_IDF(dataSize, templ);

	ifstream finIDF(dir_name + dir_TFIDF + "IDF.txt");
	ifstream finTFIDF(dir_name + dir_TFIDF + "TF-IDF.txt");
	ifstream finWordsF(dir_name + dir_TFIDF + "Words_Frequency.txt");


	int numDocs;
	finWordsF >> numDocs; // first number == numClusters; so we don't need it
	int tmp;
	finIDF >> tmp;
	finTFIDF >> tmp >> tmp;

	for (int i = 0; i < numClusters; ++i) {
		cout << "Preprocessing. Please wait    " << i << "  of  " << numClusters << " IDF\n";

		finIDF >> IDF[i];

		finWordsF >> numDocs;
		Words_Frequency[i].resize(numDocs);

		for (int j = 0; j < numDocs; ++j) {
			finWordsF >> Words_Frequency[i][j];
		}
	}

	for (int i = 0; i < dataSize; ++i) {
		cout << "Preprocessing. Please wait    " << i << "  of  " << dataSize << " TF-IDF\n";

		for (int j = 0; j < numClusters; ++j) {
			finTFIDF >> TF_IDF[i][j];
		}
	}

	int requestNum = 0;
	//ofstream ffout("bad.txt");

	while (true) {
		//requestNum++;
		cout << endl << endl << "Give request image name (number)\n";

		cin >> requestNum;
		int sgn = requestNum;
		requestNum = abs(requestNum);
		Mat request = imread(dir_name + to_string(requestNum) + ".jpg");

		while (request.empty()) {
			cout << "Request image is empty\n Try again\n";
			cin >> requestNum;
			request = imread(dir_name + to_string(requestNum) + ".jpg");
		}

		if (sgn < 0) {
			Mat blurred;
			request.copyTo(blurred);
			GaussianBlur(request, blurred, Size(5, 5), 2);
			
			Mat resrotate;
			Point2d pt(blurred.cols / 2.0, blurred.rows / 2.0);
			Mat matRot = getRotationMatrix2D(pt, 30, 1);

			warpAffine(blurred, resrotate, matRot, blurred.size());
			//request = blurred;
			request = resrotate;

			//GaussianBlur(request, blurred, Size(5, 5), 1.5);
			//request = blurred;
		}

		namedWindow("request picture");
		imshow("request picture", request);

		while (request.empty()) {
			cout << "Request image is empty\n Try again\n";
			cin >> requestNum;
			request = imread(dir_name + to_string(requestNum) + ".jpg");
		}

		// calculate request keypoints and descriptors

		vector <KeyPoint> keypoints;
		Mat descriptors;

		//SiftFeatureDetector detector(0, 3, 0.18, 10);
		SiftFeatureDetector detector(400);
		detector.detect(request, keypoints);

		SiftDescriptorExtractor extractor;
		extractor.compute(request, keypoints, descriptors);

		cout << endl << "ALL NECESSARY IS CALCULATED\n FIND CLUSTERS NUMS\n";


		/// using kmeans algo to calc distances
		
		int numWords = descriptors.rows;
		vector <int> request_clusters(numWords);

		calculation_closest_clusters(centers_of_clusters, descriptors, request_clusters);

		// count visual words in request

		vector <int> requestWords(numClusters, 0);

		//ofstream fout("__demo " + to_string(requestNum) + " clusters.txt");
		//ofstream fout("zzz " + to_string(requestNum) + " clusters.txt");


		for (int i = 0; i < numWords; ++i) {
			int word = request_clusters[i];
			requestWords[word]++;

			//fout << i << "   " << request_clusters[i] << endl;
		}

		vector <double> requestTF(numClusters, 0);
		vector <double> requestTF_IDF(numClusters, 0);

		// work out request TF and TF-IDF

		for (int word = 0; word < numClusters; ++word) {
			requestTF[word] = 1.0 * requestWords[word] / numWords;

			requestTF_IDF[word] = requestTF[word] * IDF[word];

			//fout << word << " " << requestWords[word] << "  " << requestTF_IDF[word] << endl;
		}

		// lets find the closest in Euclidian metric picture from data

		long double minDistance = DBL_MAX;
		int minDoc = -1;

		vector <bool> used_doc(dataSize, false);

		set <pair <long double, int> > min_distances;

		for (int word = 0; word < numClusters; ++word) {
			//cout << word << " search\n";
			if (requestWords[word] == 0 || IDF[word] == 0) { continue; }

			for (int i = 0; i < Words_Frequency[word].size(); ++i) {
				//int idoc = i;

				int idoc = Words_Frequency[word][i];

				//if (used_doc[idoc] || idoc >= dataEnd) { continue; }
				if (used_doc[idoc]) { continue; } // idoc was already used, so we shouldn't calc it again

				used_doc[idoc] = true;

			    long double curDist = 0; // sqrt(sum((xk - yk) ^ 2))
				long double ithLen = 0; // (xk - yk)

				//fout << endl << i + 1 << endl;

				for (int k = 0; k < numClusters; ++k) {
					ithLen = requestTF_IDF[k] - TF_IDF[idoc][k];
					//fout << "K =  " << k << " req " << requestTF_IDF[k] << "  data " << TF_IDF[idoc][k] << endl;
					curDist += ithLen * ithLen;
				}

				//curDist = sqrt(curDist);

				min_distances.insert(make_pair(curDist, idoc));

				if (curDist < minDistance) {
					minDistance = curDist;
					minDoc = idoc;

					cout << minDistance << "  doc  " << minDoc << endl;
				}
			}
		}

		if (minDoc == -1) { cout << "Smth went wrong, nothing was found\n"; continue; }

		const int numVariants = 10;
		int cnt = 0;

		set <pair <long double, int> > reranked_min;
		
		cout << endl;// << min_distances.size() << endl << endl;

		for (auto it = min_distances.begin(); cnt < numVariants && it != min_distances.end(); ++it) {
			
			cnt++;
			int idoc = (*it).second;
			cout << "Please wait.\n Image-candidates left for processing:  " << numVariants - cnt + 1 << endl;
			cout << "now  " << idoc << endl;
			Mat candidate = imread(dir_name + to_string(idoc + 1) + ".jpg");
			long double curDist =  calculation_homography_distance(request, keypoints, descriptors, candidate);

			reranked_min.insert(make_pair(curDist, idoc));
			
			cout << idoc << " deviation:  " << curDist << endl << endl;
		}
		/*
		if (abs(requestNum) < dataSize) {
			Mat candidate = imread(dir_name + to_string(abs(requestNum) + 1) + ".jpg");
			cout << endl << endl << "result for real ans   " << calculation_homography_distance(request, candidate) << endl;
		}
		*/
		cout << "The Best Picture is found!\n Min distance = " << minDistance << "\n Best doc" << minDoc + 1 << endl << endl;
		
		int ansNaive = min_distances.begin()->second;
		int ansHomo = reranked_min.begin()->second;

		cout << "min naive " << min_distances.begin()->first << " ; picture  " << ansNaive + 1 << endl;
		cout << "min homo transform " << reranked_min.begin()->first << " ; picture  " << ansHomo + 1 << endl;

		Mat resultNaive = imread(dir_name + to_string(ansNaive + 1) + ".jpg");
		Mat resultHomo = imread(dir_name + to_string(ansHomo + 1) + ".jpg");
		
		namedWindow("best naive result");
		namedWindow("best homo result");

		imshow("best naive result", resultNaive);
		imshow("best homo result", resultHomo);

		cout << endl << "Press any key...\n";

		/*
		Mat result = imread(dir_name + to_string(minDoc + 1) + ".jpg");
		namedWindow("best result");
		imshow("best result", result);
		*/
		//imwrite("__sample.jpg", result);

		/*
		//ffout << minDoc + 1 << endl;
		if (requestNum != minDoc + 1) {
			ffout << requestNum << " -- found: " << minDoc + 1 << endl;
		}
		*/

		waitKey(0);

		destroyAllWindows();

		
	}

	return 0;
}