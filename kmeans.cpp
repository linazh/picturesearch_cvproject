#include <opencv2\highgui\highgui.hpp>
#include <opencv2\core\core.hpp>
#include <opencv2\nonfree\nonfree.hpp>
#include <iostream>
#include <fstream>
#include <string>


using namespace cv;
using namespace std;

// all file writings were commented (!not to rewrite existing files)

int main() {
	string dir_name = "C:\\Users\\alina_000\\Documents\\Visual Studio 2013\\Projects\\myCVproject\\__data\\";
	string dir_descriptors = "descriptors\\";
	string dir_clustering = "clustering\\";

	ifstream finSize(dir_name + dir_descriptors + "_BigMatrixSize.txt");
	ifstream fin(dir_name + dir_descriptors + "All_descriptors.txt");


	int numRows, numCols; // Rows - number of all descriptors, Cols - amount of numbers to characterize descriptors (SIFT - 128)
	float number;

	finSize >> numRows >> numCols;
	
	Mat data(numRows, numCols, CV_32F); // Matrix of all descriptors

	// read matrix of all descriptors and put it to Mat 'data'

	for (int i = 0; i < numRows; ++i) {
		cout << i << endl;

		for (int j = 0; j < numCols; ++j) {
			fin >> number;
			data.at<float>(i, j) = number;
		}

	}

	cout << endl << data.size() << endl;
	cout << "Clusterization started\n";

	int numClusters = 1000;
	Mat labels; // Matrix of numbers of clusters for each descriptor [size: numRows X 1]
	Mat centers; // Matrix with centers of clusters [size: numClusters X numCols]

	// clustering with algorithm kmeans

	kmeans(data, numClusters, labels, TermCriteria(CV_TERMCRIT_EPS + CV_TERMCRIT_ITER, 10, 1.0), 3, KMEANS_RANDOM_CENTERS, centers);

	cout << "Clustering Succeeded\n";

	// write results into files

	//ofstream fout(dir_name + dir_clustering + "All_clusters.txt");
	//ofstream foutC(dir_name + dir_clustering + "centers_of_clusters.txt");

	// write numbers of clusters to file
	
	//fout << numRows << endl;

	for (int i = 0; i < numRows; ++i) {
		//fout << labels.at<int>(i, 0) << endl;
		
		cout << i << ")  " << labels.at<int>(i, 0) << endl;
	}

	// write centers to file
	
	//foutC << numClusters << endl; 

	for (int i = 0; i < numClusters; ++i) {
		for (int j = 0; j < numCols; ++j) {
			//foutC << centers.at<float>(i, j) << " ";
		}

		//foutC << endl;
	}

	cout << labels.size() << "  " << centers.size() << endl;
	
	return 0;
}
