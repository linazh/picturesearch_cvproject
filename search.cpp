#include <opencv2\core\core.hpp>
#include <opencv2\features2d\features2d.hpp>
#include <opencv2\nonfree\features2d.hpp>
#include <opencv2\highgui\highgui.hpp>
#include <opencv2\nonfree\nonfree.hpp>
#include <iostream>
#include <fstream>
#include <string>
#include <cmath>


using namespace cv;
using namespace std;


void calculation_closest_clusters(Mat &centers, Mat &data, vector <int> &labels) {

	const int begin = 0;
	const int end = data.rows;
	const int K = centers.rows;
	const int dims = centers.cols;

	for (int i = begin; i<end; ++i)
	{
		const float *sample = data.ptr<float>(i);
		int k_best = 0;
		double min_dist = DBL_MAX;

		for (int k = 0; k < K; k++)
		{
			const float* center = centers.ptr<float>(k);
			const double dist = normL2Sqr_(sample, center, dims);

			if (min_dist > dist)
			{
				min_dist = dist;
				k_best = k;
			}
		}

		labels[i] = k_best;
	}
}


int main() {
	string dir_name = "C:\\Users\\alina_000\\Documents\\Visual Studio 2013\\Projects\\myCVproject\\__data\\";
	string dir_descriptors = "descriptors\\";
	string dir_clustering = "clustering\\";
	string dir_TFIDF = "tf-idf\\";
	string dir_TFIDF_bin = "tf-idf_bin\\";

	ifstream finDataSize(dir_name + "_dataSize.txt");
	ifstream finClusterCenters(dir_name + dir_clustering + "centers_of_clusters.txt");

	int dataSize, numLines, numClusters; // number of clusters == number of different words
	finDataSize >> dataSize;
	finClusterCenters >> numClusters;

	int descriptPar = 128; // characteristic of descriptors (for SIFT 128 float numbers)
	Mat centers_of_clusters(numClusters, descriptPar, CV_32F);

	for (int i = 0; i < numClusters; ++i) {
		for (int j = 0; j < descriptPar; ++j) {
			finClusterCenters >> centers_of_clusters.at<float>(i, j);
		}
	}

	vector <double> templ(numClusters, 0); // "template" vector
	//vector <vector <double> > TF(dataSize, templ); // for each doc, for each word -- |word i| / |num of words in doc|
	vector <double> IDF = templ; // for each word -- log ( |dataSize| / |frequency of word i| )
	vector <vector <int> > Words_Frequency(numClusters); // for each word -- numbers of documents where the word is in
	vector <vector <double> > TF_IDF(dataSize, templ);

	ifstream finIDF(dir_name + dir_TFIDF + "IDF.txt");
	ifstream finTFIDF(dir_name + dir_TFIDF + "TF-IDF.txt");
	ifstream finWordsF(dir_name + dir_TFIDF + "Words_Frequency.txt");

	int numDocs;
	finWordsF >> numDocs; // first number == numClusters; so we don't need it
	int tmp;
	finIDF >> tmp;
	finTFIDF >> tmp >> tmp;

	for (int i = 0; i < numClusters; ++i) {
		cout << i << " IDF\n";

		finIDF >> IDF[i];

		finWordsF >> numDocs;
		Words_Frequency[i].resize(numDocs);

		for (int j = 0; j < numDocs; ++j) {
			finWordsF >> Words_Frequency[i][j];
		}
	}

	for (int i = 0; i < dataSize; ++i) {
		cout << i << " TF-IDF\n";

		for (int j = 0; j < numClusters; ++j) {
			finTFIDF >> TF_IDF[i][j];
		}
	}

	int requestNum = 0;
	//ofstream ffout("bad.txt");

	while (true) {
		//requestNum++;
		cout << "Give request number\n";

		cin >> requestNum;
		Mat request = imread(dir_name + to_string(requestNum) + ".jpg");

		while (request.empty()) {
			cout << "Request image is empty\n Try again\n";
			cin >> requestNum;
			request = imread(dir_name + to_string(requestNum) + ".jpg");
		}

		namedWindow("request picture");
		imshow("request picture", request);

		while (request.empty()) {
			cout << "Request image is empty\n Try again\n";
			cin >> requestNum;
			request = imread(dir_name + to_string(requestNum) + ".jpg");
		}

		// calculate request keypoints and descriptors

		vector <KeyPoint> keypoints;
		Mat descriptors;

		SiftFeatureDetector detector(400);
		detector.detect(request, keypoints);

		SiftDescriptorExtractor extractor;
		extractor.compute(request, keypoints, descriptors);

		cout << "ALL NECESSARY IS CALCULATED\n FIND CLUSTERS NUMS\n";


		/// using kmeans algo to calc distances

		int numWords = descriptors.rows;
		vector <int> request_clusters(numWords);

		calculation_closest_clusters(centers_of_clusters, descriptors, request_clusters);

		// count visual words in request

		vector <int> requestWords(numClusters, 0);

		//ofstream fout("__demo " + to_string(requestNum) + " clusters.txt");
		//ofstream fout("zzz " + to_string(requestNum) + " clusters.txt");


		for (int i = 0; i < numWords; ++i) {
			int word = request_clusters[i];
			requestWords[word]++;

			//fout << i << "   " << request_clusters[i] << endl;
		}

		vector <double> requestTF(numClusters, 0);
		vector <double> requestTF_IDF(numClusters, 0);

		// work out request TF and TF-IDF

		for (int word = 0; word < numClusters; ++word) {
			requestTF[word] = 1.0 * requestWords[word] / numWords;

			requestTF_IDF[word] = requestTF[word] * IDF[word];

			//fout << word << " " << requestWords[word] << "  " << requestTF_IDF[word] << endl;
		}

		// lets find the closest in Euclidian metric picture from data

		long double minDistance = DBL_MAX;
		int minDoc = -1;

		vector <bool> used_doc(dataSize, false);

		for (int word = 0; word < numClusters; ++word) {
			//cout << word << " search\n";
			if (requestWords[word] == 0) { continue; }

			for (int i = 0; i < Words_Frequency[word].size(); ++i) {
				//int idoc = i;

				int idoc = Words_Frequency[word][i];

				if (used_doc[idoc]) { continue; } // idoc was already used, so we shouldn't calc it again

				used_doc[idoc] = true;

			    long double curDist = 0; // sqrt(sum((xk - yk) ^ 2))
				long double ithLen = 0; // (xk - yk)

				//fout << endl << i + 1 << endl;

				for (int k = 0; k < numClusters; ++k) {
					ithLen = requestTF_IDF[k] - TF_IDF[idoc][k];
					//fout << "K =  " << k << " req " << requestTF_IDF[k] << "  data " << TF_IDF[idoc][k] << endl;
					curDist += ithLen * ithLen;
				}

				//curDist = sqrt(curDist);

				if (curDist < minDistance) {
					minDistance = curDist;
					minDoc = idoc;

					cout << minDistance << "  doc  " << minDoc << endl;
				}
			}
		}

		cout << "The Best Picture is found!\n Min distance = " << minDistance << "\n Best doc" << minDoc + 1 << endl << endl;

		Mat result = imread(dir_name + to_string(minDoc + 1) + ".jpg");
		namedWindow("best result");
		imshow("best result", result);

		//ffout << minDoc + 1 << endl;
		/*if (requestNum != minDoc + 1) {
			ffout << requestNum << " -- found: " << minDoc + 1 << endl;
		}
        */

		waitKey(0);

		destroyAllWindows();


	}

	return 0;
}
